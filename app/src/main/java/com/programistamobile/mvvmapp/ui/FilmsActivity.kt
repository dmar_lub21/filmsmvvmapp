package com.programistamobile.mvvmapp.ui
// wydaje sie ze swieci viewModels po migracji dla FilmsAdapter (?)
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.programistamobile.mvvmapp.R
import com.programistamobile.mvvmapp.domain.FilmModel
import com.programistamobile.mvvmapp.databinding.ActivityFilmsBinding

// what is this?
typealias OnRemoveFilmListener = (filmModel: FilmModel) -> Unit

class FilmsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityFilmsBinding
    private val viewModel: FilmsViewModel by viewModels

    private val adapter: FilmsAdapter by lazy {
        FilmsAdapter(
            onRemoveFilmListener = { viewModel.removeFilm(it) }
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFilmsBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        initRecyclerView()
        initListeners()
        initObservers()
    }

    private fun initRecyclerView() {
        binding.recyclerView.layoutManager = LinearLayoutManager(this)
        binding.recyclerView.adapter = adapter
    }

    private fun initObservers() {
        viewModel.fetchAllFilmsLiveData().observe(this, Observer { films ->
            films?.let {
                adapter.setItems(films)
            }
        })
    }

    private fun initListeners() {
        binding.saveButton.setOnClickListener {
            viewModel.storeFilm(binding.titleInput.text.toString())
        }
    }
}